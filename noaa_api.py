## Author:  Luke Caldwell
## Org: Duke University S-1 Speculative Sensation Lab
## Website: http://s-1lab.org
## License: Creative Commons BY-NC-SA 4.0
##          http://creativecommons.org/licenses/by-nc-sa/4.0/

"""
Run this from inside a python3 shell.
"""

import os
from functools import wraps
from datetime import datetime, timedelta
try:
    import requests
except ImportError:
    raise Exception('You need the requests python package. ' +
        'Run `sudo pip install requests`. ' +
        'If that throws an error, you must first install python setuptools. ' +
        'See here: https://setuptools.pypa.io/en/latest/setuptools.html#installing-setuptools')


def date_validator(func):
    @wraps(func)
    def wrapper(self, date):
        print(date)
        spl = date.split('-')
        if len(spl[0]) < 4:
            raise Exception('Please add four digit year, e.g. 2014')
        if len(spl[1]) < 2:
            spl[1] = '0' + spl[1]
        if len(spl[2]) < 2:
            spl[2] = '0' + spl[2]
        date = "-".join(spl)
        return func(self, date)
    return wrapper


def token_check(func):
    @wraps(func)
    def wrapper(self):
        if not self.token:
            raise Exception('You must first set your NOAA API token. (e.g. n.token = "XYZ")')
        return func(self)
    return wrapper


def date_check(func):
    @wraps(func)
    def wrapper(self):
        st = datetime.strptime(self.params['startdate'], '%Y-%m-%d')
        et = datetime.strptime(self.params['enddate'], '%Y-%m-%d')
        if st > et:
            print('Start date is after your end date. Changing end date to start date.')
            self.params['enddate'] = self.params['startdate']
        if et - st > timedelta(365):
            raise Exception('Time range is greater than a year. Please specify a narrower range.')
        return func(self)
    return wrapper


def data_check(func):
    @wraps(func)
    def wrapper(self):
        if not self.data:
            raise Exception('You must first successfully retrieve data from the API')
        return func(self)
    return wrapper


def limit_check(func):
    @wraps(func)
    def wrapper(self, val):
        if val > 1000:
            val = 1000
        elif val < 1:
            val = 1
        return func(self, val)
    return wrapper


def endpoint_check(func):
    @wraps(func)
    def wrapper(self, val):
        val = val.lower()
        if val not in ENDPOINTS:
            opt = "\n".join(ENDPOINTS)
            raise Exception('This is not a valid endpoint. Choose one of the following:\n{}'.format(opt))
        return func(self, val)
    return wrapper


class NoaaApi():
    token = ''
    base = 'http://www.ncdc.noaa.gov/cdo-web/api/v2/'
    endpoint = 'data'
    params = {'datasetid': 'GHCND'}
    status = ''

    def __init__(self):
        self.token = self.__get_token__()

    @date_check
    @token_check
    def get(self):
        """
        retrieve data from the API. must have token set previously.

        full response is attached to `self.response`
        json decoded data is attached to `self.data`
        response status code is assigned to `self.status_code`
        """
        self.url = os.path.join(self.base, self.endpoint)
        headers = {'token': self.token}

        r = requests.get(self.url, headers=headers, params=self.params)
        self.status = r.status_code
        if self.status == 200:
            self.response = r
            self.data = r.json()
            if not self.data:
                raise Exception('No data returned for that query. Try a different search.')
            self.meta = Metadata(self.data['metadata']['resultset'])
            print(self.meta)
        else:
            print(r.content)

    def loc(self, val):
        """
        set location to search. see example queries at
        https://www.ncdc.noaa.gov/cdo-web/webservices/v2#locations
        """

        self.params['loc'] = val
        return self

    def data_type(self, val):
        """
        specify what kinds of data to return.
        @val can be a string or list of strings for multiple datatypes
        see https://www.ncdc.noaa.gov/cdo-web/webservices/v2#dataTypes
        """

        self.params['datatypeid'] = val
        return self

    @date_validator
    def startdate(self, val):
        """
        sets starttime for date selector
        must be in format 2016-03-06
        """

        self.params['startdate'] = val
        if 'enddate' not in self.params:
            print('Automatically adding enddate of {}'.format(val))
            self.params['enddate'] = val
        return self

    @date_validator
    def enddate(self, val):
        """
        sets end time for date selector
        must be in format 2016-03-06
        """

        self.params['enddate'] = val
        if 'startdate' not in self.params:
            print('Automatically adding startdate of {}'.format(val))
            self.params['startdate'] = val
        return self

    @limit_check
    def limit(self, val):
        """
        sets the max number of records to return
        default is 25, max is 1000
        """
        self.params['limit'] = val
        return self

    @endpoint_check
    def set_endpoint(self, val):
        """
        sets API endpoint to val
        default is 'data'
        """
        self.endpoint = val
        return self

    @data_check
    def values(self):
        """
        prints out list of results
        must get results first
        """
        self.results = self.data['results']
        for result in self.results:
            print(result)

    def clear_values(self):
        """
        removes all parameters
        """
        self.params = {}
        return self

    def defaults(self):
        """
        restores default params
        """
        self.params = {'datasetid': 'GHCND'}
        self.endpoint = 'data'
        return self

    def __get_token__(self):
        """
        tries to read token from api_token file
        """
        try:
            with open('./api_token', 'r') as fi:
                token = fi.read().strip()
                if len(token) == 32:
                    print('Using token: {}'.format(token))
                    return token
                else:
                    raise TokenError()
        except (IOError, OSError, TokenError):
            print('Cannot find api_token file or your token is incorrect. Set token manually.')
            return ''

    def __str__(self):
        return str(self.params)

    def __repr__(self):
        return str(self.params)


class Metadata():
    def __init__(self, _dict):
        self.__dict__.update(_dict)

    def __str__(self):
        return str(self.__dict__)


class TokenError(Exception):
    pass


ENDPOINTS = ('datasets',
             'datacategories',
             'datatypes',
             'locationcategories',
             'locations',
             'stations',
             'data')
