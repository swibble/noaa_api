Author:  Luke Caldwell

Org: Duke University S-1 Speculative Sensation Lab

Website: http://s-1lab.org

License: Creative Commons BY-NC-SA 4.0
         http://creativecommons.org/licenses/by-nc-sa/4.0/
----------------------------------------------------------

## Setup virtualenv
This has only been tested with python 3

For best success, use in a fresh virtualenv.

Install [virtualenv](https://virtualenv.readthedocs.org/en/latest/installation.html)

Create a folder. Put the noaa_api script inside this folder. 

cd to folder in a terminal.

```
virtualenv -p python3 env
source env/bin/activate
pip install requests
pip install ipython (optional)
```
----------------

## API Token
You will need to get an API token from NOAA:
https://www.ncdc.noaa.gov/cdo-web/token

It's quick and easy. They just need an email.

For easy token access, add token to a plain text file called 'api_token' inside folder:
```
echo 'Your API Token' > api_token
```
----------------

## Example usage

```
ipython #(or python)

from noaa_api import NoaaApi

n = NoaaApi()

n.token = "Put your token here" # only necessary if you don't have the api_token file

n.loc('ZIP:98125')

n.startdate('2015-06-01')

n.enddate('2015-06-01')

n.limit(100)

n.datatype(['TMAX', 'TMIN'])

n.get()

n.values()
```